# Ansible dotfiles configuration for Regolith (Ubuntu server 22.04)

- Install git
  - `sudo apt install git`
- Install ansible
  - `sudo apt install ansible`
- Install ansible playbook(s)
  - `ansible-playbook -i hosts -K playbook-regolith.yml`
  - `ansible-playbook -i hosts -K playbook-cli.yml`
  - `ansible-playbook -i hosts -K playbook-gui.yml`